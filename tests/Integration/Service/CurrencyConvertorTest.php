<?php
declare(strict_types=1);
namespace JiriNapravnik\PrevioTest\Tests\Unit\Service;

use PHPUnit\Framework\Attributes\DataProvider;
use JiriNapravnik\PrevioTest\Enum\CurrencyEnum;
use JiriNapravnik\PrevioTest\Repository\CurrencyRepository;
use JiriNapravnik\PrevioTest\Service\Calculator\BcNumberCalculator;
use JiriNapravnik\PrevioTest\Service\CurrencyConvertor;
use JiriNapravnik\PrevioTest\ValueObject\Number;
use JiriNapravnik\PrevioTest\ValueObject\Price;
use PHPUnit\Framework\TestCase;

class CurrencyConvertorTest extends TestCase
{
	private CurrencyConvertor $currencyConvertor;
	public function setUp(): void
	{
		$this->currencyConvertor = new CurrencyConvertor(new CurrencyRepository(), new BcNumberCalculator());
	}

	#[DataProvider('getData')]
	public function testConvertEurToCzk(Price $price, CurrencyEnum $currencyTo, Price $expected){
		$actual = $this->currencyConvertor->convert($price, $currencyTo);
		$this->assertSame(floatval($expected->getPrice()->getValue()), floatval($actual->getPrice()->getValue()));
		$this->assertSame($expected->getCurrency()->value, $actual->getCurrency()->value);
	}

	public static function getData(): array{
		return [
			[Price::create(Number::fromInt(10), CurrencyEnum::EUR), CurrencyEnum::CZK, Price::create(Number::fromInt(260), CurrencyEnum::CZK)],
			[Price::create(Number::fromInt(260), CurrencyEnum::CZK), CurrencyEnum::EUR, Price::create(Number::fromInt(10), CurrencyEnum::EUR)],
			[Price::create(Number::fromFloat(1.1), CurrencyEnum::EUR), CurrencyEnum::CZK, Price::create(Number::fromFloat(28.6), CurrencyEnum::CZK)],
		];
	}
}