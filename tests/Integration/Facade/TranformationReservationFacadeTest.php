<?php
declare(strict_types=1);

namespace JiriNapravnik\PrevioTest\Tests\Integration\Facade;

use JiriNapravnik\PrevioTest\Facade\TransformReservationFacade;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class TranformationReservationFacadeTest extends TestCase
{

	private TransformReservationFacade $transformReservationFacade;

	public function setUp(): void
	{
		$containerBuilder = new ContainerBuilder();
		$loader = new YamlFileLoader($containerBuilder, new FileLocator(__DIR__ . '/../../../config/'));
		$loader->load('services.yaml');
		$containerBuilder->compile();
		$this->transformReservationFacade = $containerBuilder->get(TransformReservationFacade::class);

	}

	public function testTransformReservation()
	{
		$outputPath = sys_get_temp_dir() . '/output.json';
		$this->transformReservationFacade->transformFileReservation(__DIR__ . '/../../fixtures/reservations.json', $outputPath);

		$this->assertJsonFileEqualsJsonFile(__DIR__ . '/../../fixtures/expected-output.json', $outputPath,);
	}
}