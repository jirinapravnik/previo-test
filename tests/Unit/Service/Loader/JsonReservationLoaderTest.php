<?php

namespace JiriNapravnik\PrevioTest\Tests\Unit\Service\Loader;

use DateTimeImmutable;
use JiriNapravnik\PrevioTest\Dto\OriginalForm\DatePriceDto;
use JiriNapravnik\PrevioTest\Dto\OriginalForm\ReservationOriginalDto;
use JiriNapravnik\PrevioTest\Enum\CurrencyEnum;
use JiriNapravnik\PrevioTest\Enum\TypeEnum;
use JiriNapravnik\PrevioTest\Exception\Loader\JsonLoaderException;
use JiriNapravnik\PrevioTest\Exception\NonNumericException;
use JiriNapravnik\PrevioTest\Exception\ReservationOriginal\EmptyPricesException;
use JiriNapravnik\PrevioTest\Exception\ReservationOriginal\InvalidDateException;
use JiriNapravnik\PrevioTest\Exception\ReservationOriginal\MissingKeyException;
use JiriNapravnik\PrevioTest\Service\Loader\JsonReservationLoader;
use JiriNapravnik\PrevioTest\ValueObject\Number;
use JiriNapravnik\PrevioTest\ValueObject\Price;
use PHPUnit\Framework\TestCase;

class JsonReservationLoaderTest extends TestCase
{
	public function testLoad(){
		$loader = new JsonReservationLoader();
		$actual = $loader->load('
		[
			{
				"reservationId": 123456,
				"guest": "Petr Klas",
				"room": "Double room",
				"type": "day",
				"prices": {
				"2019-05-01": "25.3"
				},
				"currency": "EUR",
				"alreadyPaid": "50"
			}
  		]');
		$expected = new ReservationOriginalDto(
			123456,
			'Petr Klas',
			'Double room',
			TypeEnum::DAY,
			[
				new DatePriceDto(new DateTimeImmutable('2019-05-01'), Price::create(Number::fromFloat(25.3), CurrencyEnum::EUR)),
			],
			Price::create(Number::fromInt(50), CurrencyEnum::EUR),
			CurrencyEnum::EUR,
		);

		$this->assertSame($expected->getReservationId(), $actual[0]->getReservationId());
		$this->assertSame($expected->getType(), $actual[0]->getType());
		$this->assertSame($expected->getGuest(), $actual[0]->getGuest());
		$this->assertSame($expected->getRoom(), $actual[0]->getRoom());
		$this->assertSame($expected->getCurrency()->value, $actual[0]->getCurrency()->value);
		$this->assertSame($expected->getPrices()[0]->getDate()->getTimestamp(), $actual[0]->getPrices()[0]->getDate()->getTimestamp());
		$this->assertSame($expected->getPrices()[0]->getPrice()->getPrice()->getValue(), $actual[0]->getPrices()[0]->getPrice()->getPrice()->getValue());
		$this->assertSame($expected->getPrices()[0]->getPrice()->getCurrency()->value, $actual[0]->getPrices()[0]->getPrice()->getCurrency()->value);
		$this->assertSame($expected->getAlreadyPaid()->getPrice()->getValue(), $actual[0]->getAlreadyPaid()->getPrice()->getValue());
		$this->assertSame($expected->getAlreadyPaid()->getCurrency()->value, $actual[0]->getAlreadyPaid()->getCurrency()->value);
	}

	public function testInvalidJson(){
		$this->expectException(JsonLoaderException::class);
		$loader = new JsonReservationLoader();
		$loader->load('<');
	}

	public function testEmtpyPrices(){
		$this->expectException(EmptyPricesException::class);
		$loader = new JsonReservationLoader();
		$loader->load('
		[
			{
				"reservationId": 123456,
				"guest": "Petr Klas",
				"room": "Double room",
				"type": "day",
				"prices": {
				},
				"currency": "EUR",
				"alreadyPaid": "50"
			}
  		]');
	}

	public function testMissingKeyException(){
		$this->expectException(MissingKeyException::class);
		$loader = new JsonReservationLoader();
		$loader->load('
		[
			{
				"guest": "Petr Klas",
				"room": "Double room",
				"type": "day",
				"prices": {
				"2019-05-01": "25.3"
				},
				"currency": "EUR",
				"alreadyPaid": "50"
			}
  		]');
	}


	public function testNoDateInPrices(){
		$this->expectException(InvalidDateException::class);
		$loader = new JsonReservationLoader();
		$actual = $loader->load('
		[
			{
				"reservationId": 12,
				"guest": "Petr Klas",
				"room": "Double room",
				"type": "day",
				"prices": {
				"": "25.3"
				},
				"currency": "EUR",
				"alreadyPaid": "50"
			}
  		]');
	}

	public function testBadDateInPrices(){
		$this->expectException(InvalidDateException::class);
		$loader = new JsonReservationLoader();
		$actual = $loader->load('
		[
			{
				"reservationId": 12,
				"guest": "Petr Klas",
				"room": "Double room",
				"type": "day",
				"prices": {
				"2002-asd-11": "25.3"
				},
				"currency": "EUR",
				"alreadyPaid": "50"
			}
  		]');
	}
}