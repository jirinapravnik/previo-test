<?php

namespace JiriNapravnik\PrevioTest\Tests\Unit\Service\Transformer;

use JiriNapravnik\PrevioTest\Dto\NewForm\DailyNewTermDto;
use JiriNapravnik\PrevioTest\Dto\NewForm\HourlyNewTermDto;
use JiriNapravnik\PrevioTest\Dto\OriginalForm\DatePriceDto;
use JiriNapravnik\PrevioTest\Enum\CurrencyEnum;
use JiriNapravnik\PrevioTest\Enum\TypeEnum;
use JiriNapravnik\PrevioTest\Service\Transformer\TermTransformer;
use JiriNapravnik\PrevioTest\ValueObject\Number;
use JiriNapravnik\PrevioTest\ValueObject\Price;
use PHPUnit\Framework\TestCase;

class TermTransformerTest extends TestCase
{
	public function testTransformDay()
	{
		$transformer = new TermTransformer();
		$prices = [
			new DatePriceDto(new \DateTimeImmutable('2019-05-01'), Price::create(Number::fromInt(10), CurrencyEnum::EUR)),
			new DatePriceDto(new \DateTimeImmutable('2019-05-02'), Price::create(Number::fromInt(10), CurrencyEnum::EUR)),
			new DatePriceDto(new \DateTimeImmutable('2019-05-03'), Price::create(Number::fromInt(10), CurrencyEnum::EUR)),
		];

		$actual = $transformer->transform($prices, TypeEnum::DAY);
		$expected = new DailyNewTermDto(new \DateTimeImmutable('2019-05-01'), new \DateTimeImmutable('2019-05-04'));

		$this->assertInstanceOf(DailyNewTermDto::class, $actual);
		$this->assertSame($expected->getFrom()->getTimestamp(), $actual->getFrom()->getTimestamp());
		$this->assertSame($expected->getTo()->getTimestamp(), $actual->getTo()->getTimestamp());
		$this->assertSame(3, $actual->getNights());
	}

	public function testTransformHour(){
		$transformer = new TermTransformer();
		$prices = [
			new DatePriceDto(new \DateTimeImmutable('2019-05-01T23:00:00'), Price::create(Number::fromInt(10), CurrencyEnum::EUR)),
			new DatePriceDto(new \DateTimeImmutable('2019-05-02T01:00:00'), Price::create(Number::fromInt(10), CurrencyEnum::EUR)),
			new DatePriceDto(new \DateTimeImmutable('2019-05-02T00:00:00'), Price::create(Number::fromInt(10), CurrencyEnum::EUR)),
		];

		$actual = $transformer->transform($prices, TypeEnum::HOUR);
		$expected = new HourlyNewTermDto(new \DateTimeImmutable('2019-05-01T23:00:00'), new \DateTimeImmutable('2019-05-02T01:00:00'));

		$this->assertInstanceOf(HourlyNewTermDto::class, $actual);
		$this->assertSame($expected->getFrom()->getTimestamp(), $actual->getFrom()->getTimestamp());
		$this->assertSame($expected->getTo()->getTimestamp(), $actual->getTo()->getTimestamp());
		$this->assertSame(3, $actual->getHours());
	}
}