### Previo Test

V zadání bylo použít čisté PHP OOP, dovolil jsem si nad jeho rámec použít **symfony/dependency-injection**, abych nemusel řešit předávání závislostí.

Pro spuštění je třeba nainstalovat závislosti, konkrétně PHPUnit a zmiňovaný **symfony/dependency-injection**.

``` 
docker-compose  -f .docker/docker-compose.yaml run php composer install
```

Spuštění testů, které kontrolují mj. proti vzorovému zadání a výstupu: 

``` 
docker-compose  -f .docker/docker-compose.yaml run php vendor/bin/phpunit
```

"Vstupní bod" je fasáda v [src/Facade/TransformReservationFacade.php](src/Facade/TransformReservationFacade.php) použíté je vidět v integračním testu: [tests/Integration/Facade/TranformationReservationFacadeTest.php](tests/Integration/Facade/TranformationReservationFacadeTest.php). Samozřejmě otestovat by to chtělo více, ale je to spíše pro představu.
