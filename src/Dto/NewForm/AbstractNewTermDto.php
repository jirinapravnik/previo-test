<?php
declare(strict_types=1);

namespace JiriNapravnik\PrevioTest\Dto\NewForm;

use DateTimeImmutable;

abstract class AbstractNewTermDto
{

	public function __construct(
		protected DateTimeImmutable $from,
		protected DateTimeImmutable $to,
	)
	{

	}

	public function toArray(): array
	{
		return [
			'from' => $this->from,
			'to' => $this->to,
		];
	}

	public function getFrom(): DateTimeImmutable
	{
		return $this->from;
	}

	public function getTo(): DateTimeImmutable
	{
		return $this->to;
	}

	public function getDifferenceInSeconds(): int
	{
		return $this->to->getTimestamp() - $this->from->getTimestamp();
	}


}