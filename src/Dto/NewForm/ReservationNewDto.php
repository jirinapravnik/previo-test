<?php
declare(strict_types=1);

namespace JiriNapravnik\PrevioTest\Dto\NewForm;


use JiriNapravnik\PrevioTest\Enum\TypeEnum;
use JiriNapravnik\PrevioTest\Exception\ReservationOriginal\InvalidTypeException;
use JiriNapravnik\PrevioTest\ValueObject\Price;

class ReservationNewDto
{

	/**
	 * @param array<Price> $priceToBePaid
	 */
	public function __construct(
		private int $reservationId,
		private string $firstName,
		private string $lastName,
		private string $room,
		private AbstractNewTermDto $term,
		private array $priceToBePaid,
	)
	{
	}

	public function toArray(): array
	{
		return [
			'reservationId' => $this->getReservationId(),
			'firstName' => $this->getFirstName(),
			'lastName' => $this->getLastName(),
			'room' => $this->getRoom(),
			'term' => $this->getTerm()
				->toArray(),
			'priceToBePaid' => array_map(fn(Price $price) => $price->toArray(), $this->priceToBePaid),
		];
	}

	public function getReservationId(): int
	{
		return $this->reservationId;
	}

	public function getFirstName(): string
	{
		return $this->firstName;
	}

	public function getLastName(): string
	{
		return $this->lastName;
	}

	public function getRoom(): string
	{
		return $this->room;
	}

	public function getTerm(): AbstractNewTermDto
	{
		return $this->term;
	}

	public function getType(): TypeEnum
	{
		if ($this->term instanceof DailyNewTermDto) {
			return TypeEnum::DAY;
		} else {
			if ($this->term instanceof HourlyNewTermDto) {
				return TypeEnum::HOUR;
			} else {
				throw new InvalidTypeException($this->term::class);
			}
		}
	}

	public function getPriceToBePaid(): array
	{
		return $this->priceToBePaid;
	}


}