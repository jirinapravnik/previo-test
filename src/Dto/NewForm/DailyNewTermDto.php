<?php
declare(strict_types=1);

namespace JiriNapravnik\PrevioTest\Dto\NewForm;

class DailyNewTermDto extends AbstractNewTermDto
{

	public function toArray(): array
	{
		return [
			...parent::toArray(),
			'nights' => $this->getNights(),
		];
	}

	public function getNights(): int
	{
		return (int)floor($this->getDifferenceInSeconds() / 3600 / 24);
	}


}