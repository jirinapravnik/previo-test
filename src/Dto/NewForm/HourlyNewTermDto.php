<?php
declare(strict_types=1);

namespace JiriNapravnik\PrevioTest\Dto\NewForm;

class HourlyNewTermDto extends AbstractNewTermDto
{

	public function toArray(): array
	{
		return [
			...parent::toArray(),
			'hours' => $this->getHours(),
		];
	}


	public function getHours(): int
	{
		return (int)floor(($this->getDifferenceInSeconds() / 3600)) + 1;
	}


}