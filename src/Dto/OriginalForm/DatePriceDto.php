<?php
declare(strict_types=1);

namespace JiriNapravnik\PrevioTest\Dto\OriginalForm;

use DateTimeImmutable;
use JiriNapravnik\PrevioTest\Enum\CurrencyEnum;
use JiriNapravnik\PrevioTest\Exception\ReservationOriginal\InvalidDateException;
use JiriNapravnik\PrevioTest\ValueObject\Number;
use JiriNapravnik\PrevioTest\ValueObject\Price;

class DatePriceDto
{

	public function __construct(
		private DateTimeImmutable $date,
		private Price $price,
	)
	{

	}

	public static function create(
		string $date,
		string $price,
		CurrencyEnum $currency,
	) {
		self::ensureDateIsValid($date);
		return new self(
			new DateTimeImmutable($date),
			Price::create(Number::fromString($price), $currency)
		);
	}

	private static function ensureDateIsValid(string $date){
		if(strlen($date) === 0 || strtotime($date) === false){
			throw new InvalidDateException($date);
		}
	}

	public function getDate(): DateTimeImmutable
	{
		return $this->date;
	}

	public function getPrice(): Price
	{
		return $this->price;
	}

}