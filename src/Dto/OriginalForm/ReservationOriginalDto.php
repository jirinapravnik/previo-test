<?php
declare(strict_types=1);

namespace JiriNapravnik\PrevioTest\Dto\OriginalForm;

use JiriNapravnik\PrevioTest\Enum\CurrencyEnum;
use JiriNapravnik\PrevioTest\Enum\TypeEnum;
use JiriNapravnik\PrevioTest\Exception\ReservationOriginal\EmptyPricesException;
use JiriNapravnik\PrevioTest\ValueObject\Number;
use JiriNapravnik\PrevioTest\ValueObject\Price;

class ReservationOriginalDto
{

	public function __construct(
		private int $reservationId,
		private string $guest,
		private string $room,
		private TypeEnum $type,
		private array $prices,
		private Price $alreadyPaid,
		private CurrencyEnum $currency,
	)
	{

	}

	public static function create(
		int $reservationId,
		string $guest,
		string $room,
		string $type,
		array $prices,
		string $alreadyPaid,
		string $currency,
	): self{
		self::ensureNonEmptyPrices($prices);

		$currency = CurrencyEnum::create($currency);
		$prices = self::preparePrices($prices, $currency);
		return new self(
			$reservationId,
			$guest,
			$room,
			TypeEnum::create($type),
			$prices,
			Price::create(Number::fromString($alreadyPaid), $currency),
			$currency,
		);
	}

	private static function preparePrices($prices, CurrencyEnum $currency){
		$ret = [];
		foreach ($prices as $date => $price) {
			$ret[] = DatePriceDto::create($date, $price, $currency);
		}
		return $ret;
	}

	private static function ensureNonEmptyPrices(array $prices){
		if(count($prices) === 0){
			throw new EmptyPricesException('At least one prices should be in reservation.');
		}
	}

	public function getFirstname(): string
	{
		return explode(' ', $this->guest)[0];
	}

	public function getSurname(): string
	{
		$expl = explode(' ', $this->guest);
		return implode(' ', array_slice($expl, 1));
	}

	public function getReservationId(): int
	{
		return $this->reservationId;
	}

	public function getGuest(): string
	{
		return $this->guest;
	}

	public function getRoom(): string
	{
		return $this->room;
	}

	public function getType(): TypeEnum
	{
		return $this->type;
	}

	/**
	 * @return array<DatePriceDto>
	 */
	public function getPrices(): array
	{
		return $this->prices;
	}

	public function getCurrency(): CurrencyEnum
	{
		return $this->currency;
	}

	public function getAlreadyPaid(): Price
	{
		return $this->alreadyPaid;
	}


}