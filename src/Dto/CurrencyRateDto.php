<?php
declare(strict_types=1);

namespace JiriNapravnik\PrevioTest\Dto;

use JiriNapravnik\PrevioTest\Enum\CurrencyEnum;
use JiriNapravnik\PrevioTest\ValueObject\Number;

class CurrencyRateDto
{

	public function __construct(
		private CurrencyEnum $from,
		private CurrencyEnum $to,
		private Number $rate,
	)
	{

	}

	public function getFrom(): CurrencyEnum
	{
		return $this->from;
	}

	public function getTo(): CurrencyEnum
	{
		return $this->to;
	}

	public function getRate(): Number
	{
		return $this->rate;
	}


}