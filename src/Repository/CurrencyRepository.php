<?php
declare(strict_types=1);

namespace JiriNapravnik\PrevioTest\Repository;

use JiriNapravnik\PrevioTest\Contracts\ICalculator;
use JiriNapravnik\PrevioTest\Dto\CurrencyRateDto;
use JiriNapravnik\PrevioTest\Enum\CurrencyEnum;
use JiriNapravnik\PrevioTest\Exception\CurrencyConvertor\CurrencyRateNotFoundException;
use JiriNapravnik\PrevioTest\ValueObject\Number;

class CurrencyRepository
{

	public function getExchangeRate(CurrencyEnum $from, CurrencyEnum $to): CurrencyRateDto
	{
		foreach ($this->getAllRates() as $rate) {
			if (
				($from === $rate->getFrom() && $to === $rate->getTo())
				||
				($to === $rate->getFrom() && $from === $rate->getTo())
			) {
				return $rate;
			}
		}
		throw new CurrencyRateNotFoundException();
	}

	/**
	 * @return CurrencyRateDto[]
	 */
	private function getAllRates(): array
	{
		return [
			new CurrencyRateDto(CurrencyEnum::EUR, CurrencyEnum::CZK, Number::fromInt(26)),
		];
	}
}