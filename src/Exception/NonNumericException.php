<?php
declare(strict_types=1);

namespace JiriNapravnik\PrevioTest\Exception;

use InvalidArgumentException;

class NonNumericException extends InvalidArgumentException
{
	public function __construct(string $key){
		parent::__construct($key . ' is not in numeric format.');
	}
}