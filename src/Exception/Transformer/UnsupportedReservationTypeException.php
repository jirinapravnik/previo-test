<?php
declare(strict_types=1);

namespace JiriNapravnik\PrevioTest\Exception\Transformer;

use InvalidArgumentException;
use JiriNapravnik\PrevioTest\Enum\TypeEnum;

class UnsupportedReservationTypeException extends InvalidArgumentException
{
	public function __construct(TypeEnum $type)
	{
		parent::__construct('Unsupported reservation type: ' . $type->value . '.');
	}
}