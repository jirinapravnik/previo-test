<?php
declare(strict_types=1);

namespace JiriNapravnik\PrevioTest\Exception\Loader;

use InvalidArgumentException;

class JsonLoaderException extends InvalidArgumentException
{

}