<?php
declare(strict_types=1);

namespace JiriNapravnik\PrevioTest\Exception\CurrencyConvertor;

use InvalidArgumentException;

class CurrencyRateNotFoundException extends InvalidArgumentException
{


}