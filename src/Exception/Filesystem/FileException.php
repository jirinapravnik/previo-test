<?php
declare(strict_types=1);

namespace JiriNapravnik\PrevioTest\Exception\Filesystem;

use InvalidArgumentException;

class FileException extends InvalidArgumentException
{


}