<?php
declare(strict_types=1);

namespace JiriNapravnik\PrevioTest\Exception\Filesystem;

class FileNotFoundException extends FileException
{

	public function __construct(string $path)
	{
		parent::__construct('File ' . $path . ' not exists.');
	}

}