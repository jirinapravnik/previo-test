<?php
declare(strict_types=1);

namespace JiriNapravnik\PrevioTest\Exception\ReservationOriginal;

use InvalidArgumentException;
use JiriNapravnik\PrevioTest\Enum\CurrencyEnum;

class InvalidCurrencyException extends InvalidArgumentException
{

	public function __construct(string $currency)
	{
		parent::__construct('Invalid currency: ' . $currency . '. Only these currenies are supported: ' . implode(', ', array_map(fn(CurrencyEnum $currency) => $currency->value, CurrencyEnum::cases())) . '.');
	}

}