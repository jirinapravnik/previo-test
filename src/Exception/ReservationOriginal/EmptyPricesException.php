<?php
declare(strict_types=1);

namespace JiriNapravnik\PrevioTest\Exception\ReservationOriginal;

use InvalidArgumentException;

class EmptyPricesException extends InvalidArgumentException
{
}