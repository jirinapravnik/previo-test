<?php
declare(strict_types=1);

namespace JiriNapravnik\PrevioTest\Exception\ReservationOriginal;

use InvalidArgumentException;
use JiriNapravnik\PrevioTest\Enum\TypeEnum;

class InvalidTypeException extends InvalidArgumentException
{

	public function __construct(string $currency)
	{
		parent::__construct('Invalid type of reservation: ' . $currency . '. Only these types are supported: ' . implode(', ', array_map(fn(TypeEnum $type) => $currency->value, TypeEnum::cases())) . '.');
	}

}