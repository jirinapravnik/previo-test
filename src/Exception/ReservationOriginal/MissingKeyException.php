<?php
declare(strict_types=1);

namespace JiriNapravnik\PrevioTest\Exception\ReservationOriginal;

use InvalidArgumentException;

class MissingKeyException extends InvalidArgumentException
{
	public function __construct(string $key)
	{
		parent::__construct('Missing key: ' . $key . ' in JSON.');
	}
}