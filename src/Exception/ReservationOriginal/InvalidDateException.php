<?php
declare(strict_types=1);

namespace JiriNapravnik\PrevioTest\Exception\ReservationOriginal;

use InvalidArgumentException;

class InvalidDateException extends InvalidArgumentException
{
	public function __construct(string $key)
	{
		parent::__construct('Invalid date: ' . $key . ' in JSON.');
	}
}