<?php
declare(strict_types=1);

namespace JiriNapravnik\PrevioTest\Service;


use JiriNapravnik\PrevioTest\Exception\Filesystem\FileException;
use JiriNapravnik\PrevioTest\Exception\Filesystem\FileNotFoundException;
use JiriNapravnik\PrevioTest\Exception\Filesystem\FileNotReadableException;

class FileSystem
{

	public function loadFile(string $path): string
	{
		$this->ensureFileExists($path);
		$ret = file_get_contents($path);
		if ($ret === false) {
			throw new FileException('Can\'t get file on path: ' . $path);
		}
		return $ret;
	}

	private function ensureFileExists($path)
	{
		if (!file_exists($path)) {
			throw new FileNotFoundException($path);
		}
		if (!is_readable($path)) {
			throw new FileNotReadableException($path);
		}
	}

	public function saveFile(string $content, string $path): void
	{
		$this->createDirIfNotExists(dirname($path));
		$ret = file_put_contents($path, $content);

		if ($ret === false) {
			throw new FileException('Can\'t save file to path: ' . $path);
		}
	}

	private function createDirIfNotExists(string $dir)
	{
		$permission = 0666;
		if (!file_exists($dir)) {
			mkdir($dir, $permission);
		}
		chmod($dir, $permission);
	}
}