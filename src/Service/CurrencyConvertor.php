<?php
declare(strict_types=1);

namespace JiriNapravnik\PrevioTest\Service;

use JiriNapravnik\PrevioTest\Contracts\ICalculator;
use JiriNapravnik\PrevioTest\Enum\CurrencyEnum;
use JiriNapravnik\PrevioTest\Repository\CurrencyRepository;
use JiriNapravnik\PrevioTest\ValueObject\Price;

class CurrencyConvertor
{

	public function __construct(
		private CurrencyRepository $currencyRepository,
		private ICalculator $calculator,
	)
	{
	}

	/**
	 * @param Price $price
	 * @return array<Price>
	 */
	public function convertToSupportedCurrencies(Price $price): array
	{
		$ret = [$price];
		foreach (CurrencyEnum::cases() as $currency) {
			if ($currency === $price->getCurrency()) {
				continue;
			}

			$ret[] = $this->convert($price, $currency);
		}

		//sort alphabeticaly by name of currency
		usort($ret, fn(Price $first, Price $second) => $first->getCurrency()->value <=> $second->getCurrency()->value);
		return $ret;
	}

	public function convert(Price $price, CurrencyEnum $toCurrency): Price
	{
		$rate = $this->currencyRepository->getExchangeRate($price->getCurrency(), $toCurrency);
		if($price->getCurrency() === $rate->getFrom()){
			$newPrice = $this->calculator->multiple($price->getPrice(), $rate->getRate());
		} else {
			$newPrice = $this->calculator->divide($price->getPrice(), $rate->getRate());
		}

		return Price::create($newPrice, $toCurrency);
	}
}