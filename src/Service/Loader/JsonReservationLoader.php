<?php
declare(strict_types=1);

namespace JiriNapravnik\PrevioTest\Service\Loader;

use JiriNapravnik\PrevioTest\Contracts\IReservationLoader;
use JiriNapravnik\PrevioTest\Dto\OriginalForm\ReservationOriginalDto;
use JiriNapravnik\PrevioTest\Exception\Loader\JsonLoaderException;
use JiriNapravnik\PrevioTest\Exception\ReservationOriginal\MissingKeyException;

class JsonReservationLoader implements IReservationLoader
{

	/**
	 * @return array<ReservationOriginalDto>
	 */
	public function load(string $content): array
	{
		$reservations = json_decode($content, true);
		if ($reservations === null) {
			throw new JsonLoaderException('Invalid json. Error number: ' . json_last_error());
		}

		$ret = [];

		foreach ($reservations as $reservation) {
			$this->ensureNonMissing($reservation);
			$ret[] = ReservationOriginalDto::create(
				$reservation['reservationId'],
				$reservation['guest'],
				$reservation['room'],
				$reservation['type'],
				$reservation['prices'],
				$reservation['alreadyPaid'],
				$reservation['currency'],
			);
		}
		return $ret;
	}


	private function ensureNonMissing(array $data)
	{
		foreach(['reservationId', 'guest', 'room', 'type', 'prices', 'currency', 'alreadyPaid'] as $key){
			if(!array_key_exists($key, $data)){
				throw new MissingKeyException($key);
			}
		}

	}

}