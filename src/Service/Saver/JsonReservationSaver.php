<?php
declare(strict_types=1);

namespace JiriNapravnik\PrevioTest\Service\Saver;

use JiriNapravnik\PrevioTest\Contracts\IReservationSaver;
use JiriNapravnik\PrevioTest\Dto\NewForm\ReservationNewDto;
use JiriNapravnik\PrevioTest\Enum\TypeEnum;
use JiriNapravnik\PrevioTest\Exception\ReservationOriginal\InvalidTypeException;

class JsonReservationSaver implements IReservationSaver
{


	/**
	 * @param array<ReservationNewDto> $reservationNewDtos
	 * @return string
	 */
	public function save(array $reservationNewDtos): string
	{
		$toJson = [];
		foreach ($reservationNewDtos as $reservationNewDto) {
			$reservation = $reservationNewDto->toArray();
			$reservation = $this->formatDates($reservation, $reservationNewDto->getType());
			$reservation = $this->formatPriceToBePaid($reservation);
			$toJson[] = $reservation;
		}
		return json_encode($toJson);
	}

	private function formatDates(array $reservationNew, TypeEnum $type): array
	{
		$format = match ($type) {
			TypeEnum::DAY => 'Y-m-d',
			TypeEnum::HOUR => 'Y-m-d\TH:i:s',
		};

		$reservationNew['term']['from'] = $reservationNew['term']['from']->format($format);
		$reservationNew['term']['to'] = $reservationNew['term']['to']->format($format);
		return $reservationNew;
	}

	private function formatPriceToBePaid($reservation): array
	{
		$reservation['priceToBePaid'] = array_map(function (array $value) {
			$value['price'] = (string)round(floatval($value['price']->getValue()), 2);
			return $value;
		}, $reservation['priceToBePaid']);
		return $reservation;
	}
}