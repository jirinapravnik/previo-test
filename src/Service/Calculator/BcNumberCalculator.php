<?php
declare(strict_types=1);

namespace JiriNapravnik\PrevioTest\Service\Calculator;

use JiriNapravnik\PrevioTest\Contracts\ICalculator;
use JiriNapravnik\PrevioTest\ValueObject\Number;

class BcNumberCalculator implements ICalculator
{

	const SCALE = 10;

	public function add(Number $first, Number $second): Number
	{
		return Number::fromString(bcadd($first->getValue(), $second->getValue(), self::SCALE));
	}

	public function substract(Number $number, Number $substractor): Number
	{
		return Number::fromString(bcsub($number->getValue(), $substractor->getValue(), self::SCALE));
	}

	public function divide(Number $number, Number $divisor): Number
	{
		return Number::fromString(bcdiv($number->getValue(), $divisor->getValue(), self::SCALE));
	}

	public function multiple(Number $number, Number $multipler): Number
	{
		return Number::fromString(bcmul($number->getValue(), $multipler->getValue(), self::SCALE));
	}
}