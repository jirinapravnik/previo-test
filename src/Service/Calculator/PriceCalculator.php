<?php
declare(strict_types=1);

namespace JiriNapravnik\PrevioTest\Service\Calculator;

use JiriNapravnik\PrevioTest\Contracts\ICalculator;
use JiriNapravnik\PrevioTest\Dto\OriginalForm\DatePriceDto;
use JiriNapravnik\PrevioTest\Enum\CurrencyEnum;
use JiriNapravnik\PrevioTest\Service\CurrencyConvertor;
use JiriNapravnik\PrevioTest\ValueObject\Number;
use JiriNapravnik\PrevioTest\ValueObject\Price;

class PriceCalculator
{

	public function __construct(private CurrencyConvertor $currencyConvertor, private ICalculator $calculator)
	{

	}

	/**
	 * @param array<DatePriceDto>
	 * @return array<Price>
	 */
	public function calculatePriceToBePaid(array $datePrices, Price $alreadyPaid, CurrencyEnum $originalCurrency): array
	{
		$toBePaid = $this->calculator->substract($this->getSumPrices($datePrices), $alreadyPaid->getPrice());
		return $this->currencyConvertor->convertToSupportedCurrencies(Price::create($toBePaid, $originalCurrency));
	}

	/**
	 * @param array<DatePriceDto> $datePrices
	 * @return Number
	 */
	public function getSumPrices(array $datePrices): Number
	{
		$sum = Number::fromInt(0);
		foreach ($datePrices as $datePrice) {
			$sum = $this->calculator->add(
				$sum,
				$datePrice->getPrice()->getPrice()
			);
		}
		return $sum;
	}
}