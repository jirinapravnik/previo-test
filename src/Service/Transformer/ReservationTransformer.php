<?php
declare(strict_types=1);

namespace JiriNapravnik\PrevioTest\Service\Transformer;


use JiriNapravnik\PrevioTest\Dto\NewForm\ReservationNewDto;
use JiriNapravnik\PrevioTest\Dto\OriginalForm\ReservationOriginalDto;
use JiriNapravnik\PrevioTest\Service\Calculator\PriceCalculator;

class ReservationTransformer
{

	public function __construct(
		private TermTransformer $termTransformer,
		private PriceCalculator $priceCalculator,
	)
	{

	}

	/**
	 * @param array<ReservationOriginalDto>
	 * @return array<ReservationNewDto>
	 */
	public function transformReservations(array $originalDtos): array
	{
		$ret = [];
		foreach ($originalDtos as $originalDto) {
			$ret[] = $this->transformReservation($originalDto);
		}
		return $ret;
	}

	public function transformReservation(ReservationOriginalDto $originalDto): ReservationNewDto
	{
		return new ReservationNewDto(
			$originalDto->getReservationId(),
			$originalDto->getFirstname(),
			$originalDto->getSurname(),
			$originalDto->getRoom(),
			$this->termTransformer->transform($originalDto->getPrices(), $originalDto->getType()),
			$this->priceCalculator->calculatePriceToBePaid($originalDto->getPrices(), $originalDto->getAlreadyPaid(), $originalDto->getCurrency()),
		);
	}
}