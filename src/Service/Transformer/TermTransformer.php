<?php
declare(strict_types=1);

namespace JiriNapravnik\PrevioTest\Service\Transformer;


use JiriNapravnik\PrevioTest\Dto\NewForm\AbstractNewTermDto;
use JiriNapravnik\PrevioTest\Dto\NewForm\DailyNewTermDto;
use JiriNapravnik\PrevioTest\Dto\NewForm\HourlyNewTermDto;
use JiriNapravnik\PrevioTest\Dto\OriginalForm\DatePriceDto;
use JiriNapravnik\PrevioTest\Enum\TypeEnum;
use JiriNapravnik\PrevioTest\Exception\Transformer\UnsupportedReservationTypeException;

class TermTransformer
{

	/**
	 * @param array<DatePriceDto> $prices
	 */
	public function transform(array $prices, TypeEnum $type): AbstractNewTermDto
	{
		$this->sortPricesByDate($prices);
		if ($type === TypeEnum::DAY) {
			return $this->transformDailyReservations($prices);
		} elseif ($type === TypeEnum::HOUR) {
			return $this->transformHourlyReservations($prices);
		} else {
			throw new UnsupportedReservationTypeException($type);
		}
	}

	private function transformDailyReservations(array $prices): DailyNewTermDto
	{
		$from = $prices[array_key_first($prices)]->getDate();
		$to = $prices[array_key_last($prices)]->getDate()->modify('+1 day');

		return new DailyNewTermDto($from, $to);
	}

	private function transformHourlyReservations(array $prices): HourlyNewTermDto
	{
		$from = $prices[array_key_first($prices)]->getDate();
		$to = $prices[array_key_last($prices)]->getDate();
		return new HourlyNewTermDto($from, $to);
	}

	private function sortPricesByDate(array &$prices){
		usort($prices, fn(DatePriceDto $first, DatePriceDto $second) => $first->getDate() <=> $second->getDate());
	}
}