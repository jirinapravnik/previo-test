<?php
declare(strict_types=1);

namespace JiriNapravnik\PrevioTest\Enum;

use JiriNapravnik\PrevioTest\Exception\ReservationOriginal\InvalidTypeException;
use ValueError;

enum TypeEnum: string
{

	case DAY = 'day';
	case HOUR = 'hour';

	public static function create(string $type): self
	{
		try {
			return TypeEnum::from($type);
		} catch (ValueError $er) {
			throw new InvalidTypeException($type);
		}
	}
}
