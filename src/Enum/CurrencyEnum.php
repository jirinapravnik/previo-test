<?php
declare(strict_types=1);

namespace JiriNapravnik\PrevioTest\Enum;

use JiriNapravnik\PrevioTest\Exception\ReservationOriginal\InvalidCurrencyException;
use ValueError;

enum CurrencyEnum: string
{

	case CZK = 'CZK';
	case EUR = 'EUR';

	public static function create(string $currency): self
	{
		try {
			return CurrencyEnum::from($currency);
		} catch (ValueError $er) {
			throw new InvalidCurrencyException($currency);
		}
	}
}
