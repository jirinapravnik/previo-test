<?php
declare(strict_types=1);

namespace JiriNapravnik\PrevioTest\ValueObject;

use JiriNapravnik\PrevioTest\Exception\NonNumericException;

class Number
{

	private function __construct(
		private string $value,
	)
	{
		$this->ensureIsNumeric($value);
	}

	private function ensureIsNumeric(string $value)
	{
		if (!is_numeric($value)) {
			throw new NonNumericException($value);
		}
	}

	public static function fromString(string $value): Number
	{
		return new Number($value);
	}

	public static function fromInt(int $value): Number
	{
		return new Number((string)$value);
	}

	public static function fromFloat(float $value): Number
	{
		return new Number((string)$value);
	}

	public function getValue(): string
	{
		return $this->value;
	}
}