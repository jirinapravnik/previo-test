<?php
declare(strict_types=1);

namespace JiriNapravnik\PrevioTest\ValueObject;


use JiriNapravnik\PrevioTest\Enum\CurrencyEnum;

class Price
{

	private function __construct(
		private Number $price,
		private CurrencyEnum $currency,
	)
	{

	}

	public static function create(Number $price, CurrencyEnum $currency): self
	{
		return new self($price, $currency);
	}

	public function toArray()
	{
		return [
			'currency' => $this->currency->value,
			'price' => $this->price,
		];
	}

	public function getPrice(): Number
	{
		return $this->price;
	}

	public function getCurrency(): CurrencyEnum
	{
		return $this->currency;
	}


}