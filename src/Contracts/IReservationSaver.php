<?php
declare(strict_types=1);

namespace JiriNapravnik\PrevioTest\Contracts;


use JiriNapravnik\PrevioTest\Dto\NewForm\ReservationNewDto;

interface IReservationSaver
{

	/**
	 * @param array<ReservationNewDto> $reservationNewDtos
	 * @return string
	 */
	public function save(array $reservationNewDtos): string;
}