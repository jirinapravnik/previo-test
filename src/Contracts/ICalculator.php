<?php
declare(strict_types=1);

namespace JiriNapravnik\PrevioTest\Contracts;

use JiriNapravnik\PrevioTest\ValueObject\Number;

interface ICalculator
{

	public function add(Number $first, Number $second): Number;

	public function substract(Number $number, Number $substractor): Number;

	public function divide(Number $number, Number $divisor): Number;

	public function multiple(Number $number, Number $multipler): Number;
}