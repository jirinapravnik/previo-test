<?php
declare(strict_types=1);

namespace JiriNapravnik\PrevioTest\Contracts;


use JiriNapravnik\PrevioTest\Dto\OriginalForm\ReservationOriginalDto;

interface IReservationLoader
{

	/**
	 * @return array<ReservationOriginalDto>
	 */
	public function load(string $content): array;
}