<?php
declare(strict_types=1);

namespace JiriNapravnik\PrevioTest\Facade;

use JiriNapravnik\PrevioTest\Contracts\IReservationLoader;
use JiriNapravnik\PrevioTest\Contracts\IReservationSaver;
use JiriNapravnik\PrevioTest\Service\FileSystem;
use JiriNapravnik\PrevioTest\Service\Transformer\ReservationTransformer;

class TransformReservationFacade
{

	public function __construct(
		private FileSystem $fileSystem,
		private IReservationLoader $reservationLoader,
		private ReservationTransformer $reservationTransformer,
		private IReservationSaver $reservationSaver,
	)
	{

	}

	public function transformFileReservation(string $fromPath, string $savePath)
	{
		$originalReservations = $this->reservationLoader->load($this->fileSystem->loadFile($fromPath));
		$newDto = $this->reservationTransformer->transformReservations($originalReservations);
		$this->fileSystem->saveFile($this->reservationSaver->save($newDto), $savePath);
	}
}